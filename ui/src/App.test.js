import React from 'react';
import { shallow } from 'enzyme';
import App from './components/App';
import SearchAppBar from './components/SearchAppBar.js';
import ServersTable from './components/ServersTable';

it('renders without crashing', () => {
  shallow(<App />);
});

it('includes SearchAppBar', () => {
  const app = shallow(<App />);
  expect(app.containsMatchingElement(<SearchAppBar />)).toEqual(true)
});

it('includes SeerversTable', () => {
  const app = shallow(<App />);
  expect(app.containsMatchingElement(<ServersTable searchString="" />)).toEqual(true)
});
