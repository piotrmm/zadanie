import React from 'react';
import { connect } from 'react-redux';
import { removeErrorAlert } from '../actions/index';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

function AlertDialogConnected(props) {
  const { error, removeErrorAlert } = props;
  const [open, setOpen] = React.useState(error ? true : false);

  const handleClose = () => {
    removeErrorAlert();
    setOpen(false);
  };

  return (
    <div>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{"Error"}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            {error}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Close
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

const mapStateToProps = state => {
  return {
    error: state.error,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    removeErrorAlert: () => dispatch(removeErrorAlert()),
  }
}

const AlertDialog = connect(
  mapStateToProps,
  mapDispatchToProps
)(AlertDialogConnected);

export default AlertDialog;
