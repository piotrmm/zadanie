import React from 'react';
import { connect } from 'react-redux';
import { setStatusServer } from '../actions/index';
import IconButton from '@material-ui/core/IconButton';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import MoreVertIcon from '@material-ui/icons/MoreVert';

function StatusMenuConnected(props) {
  const [anchorEl, setAnchorEl] = React.useState(null);

  const { statusServer, setStatusServer } = props;

  const handleClick = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleSelect = (status, parametr) => {
    setStatusServer({
      id: statusServer.id,
      name: statusServer.name,
      status,
    }, parametr);
    setAnchorEl(null);
  };

  return (
    <div>
      <IconButton
        aria-label="status"
        aria-controls="status-menu"
        aria-haspopup="true"
        onClick={handleClick}
      >
        <MoreVertIcon />
      </IconButton>
      <Menu
        id="status-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        {statusServer.status === 'OFFLINE'
          ? <MenuItem onClick={() => handleSelect('ONLINE', 'on')}>Turn on</MenuItem>
          : <div>
              <MenuItem onClick={() => handleSelect('OFFLINE', 'off')}>Turn off</MenuItem>
              <MenuItem onClick={() => handleSelect('REBOOT', 'reboot')}>Reboot</MenuItem>
            </div>
        }
      </Menu>
    </div>
  );
}

function mapDispatchToProps(dispatch) {
  return {
	  setStatusServer: (status, parametr) => dispatch(setStatusServer(status, parametr)),
  }
}

const StatusMenu = connect(
  null,
  mapDispatchToProps
)(StatusMenuConnected);

export default StatusMenu;
