import React, { Component } from 'react';
import SearchAppBar from './SearchAppBar.js';
import ServersTable from './ServersTable';

class App extends Component {
  state = {
    searchString: ''
  }

  handleChange = (searchString) => {
    this.setState({
      searchString,
    })
  }

  render() {
    const { searchString } = this.state;

    return (
      <div>
        <SearchAppBar onChange={this.handleChange} />
        <ServersTable searchString={searchString} />
      </div>
    );
  }
}

export default App;
