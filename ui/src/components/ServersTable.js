import React from 'react';
import { connect } from 'react-redux';
import { fetchServers } from '../actions/index';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import StatusMenu from './StatusMenu';
import AlertDialog from './AlertDialog';

const useStyles = makeStyles({
  root: {
    width: '100%',
    overflowX: 'auto',
  },
  table: {
    maxWidth: 800,
    margin: 'auto',
  },
});

function ServersTableConnected(props) {
  const classes = useStyles();
  const { fetchServers } = props;

  React.useEffect(() => {
    fetchServers();
  }, [fetchServers]);

  const { servers, searchString } = props;

  return (
    <>
      <AlertDialog />
      <Paper className={classes.root}>
        <Table className={classes.table} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>Name</TableCell>
              <TableCell>Status</TableCell>
              <TableCell align="right"></TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {servers.filter(server =>
              server.name.toLowerCase().search(searchString.toLowerCase()) !== -1
            ).map(server => {
              const { id, name, status } = server;

              return (
              <TableRow key={id}>
                <TableCell>{name}</TableCell>
                <TableCell>{status}</TableCell>
                <TableCell align="right">
                  <StatusMenu statusServer={server} />
                </TableCell>
              </TableRow>
            )})}
          </TableBody>
        </Table>
      </Paper>
    </>
  );
}

const mapStateToProps = state => {
  return {
    servers: state.servers,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    fetchServers: () => dispatch(fetchServers()),
  }
}

const ServersTable = connect(
  mapStateToProps,
  mapDispatchToProps
)(ServersTableConnected);

export default ServersTable;
