import {
  FETCH_DATA_BEGIN,
  FETCH_DATA_SUCCESS,
  FETCH_DATA_FAILURE,
  FETCH_STATUS_BEGIN,
  FETCH_STATUS_SUCCESS,
  FETCH_STATUS_FAILURE,
  REMOVE_ERROR,
} from '../constants/action-types';

const API = 'http://localhost:4454/servers';

function handleErrors(response) {
  if (!response.ok) {
    throw Error(response.statusText);
  }
  return response;
}

export function fetchServers() {
  return dispatch => {
    dispatch(fetchServersBegin());
    return fetch(API)
      .then(handleErrors)
      .then(res => res.json())
      .then(json => {
        dispatch(fetchServersSuccess(json));
        return json;
      })
      .catch(error => dispatch(fetchServersFailure(error)));
  };
}

export const fetchServersBegin = () => ({
  type: FETCH_DATA_BEGIN,
});

export const fetchServersSuccess = servers => ({
  type: FETCH_DATA_SUCCESS,
  payload: servers,
});

export const fetchServersFailure = error => ({
  type: FETCH_DATA_FAILURE,
  payload: { error },
});

async function getSttatusServer(url) {
  const response = await fetch(url);
  const data = await response.json();
  return data;
}

export function setStatusServer(status, parametr) {
  return dispatch => {
    dispatch(setStatusServerBegin());
    return fetch(`${API}/${status.id}/${parametr}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
      body: JSON.stringify(status),
    })
      .then(handleErrors)
      .then(res => res.json())
      .then(json => {
        dispatch(setStatusServerSuccess(json));
        if (json.status === 'REBOOTING') {
          setTimeout(() => {
            function pingServer() {
              getSttatusServer(`${API}/${status.id}`)
                .then(data => {
                  dispatch(setStatusServerSuccess(data));
                  if (data.status === 'REBOOTING') pingServer();
                }); 
            }
            pingServer();
          }, 1000);
        }

        return json;
      })
      .catch(error => dispatch(setStatusServerFailure(error)));
  };
}

export const setStatusServerBegin = () => ({
  type: FETCH_STATUS_BEGIN,
});

export const setStatusServerSuccess = server => {
  const { id, name, status } = server;

  return ({
    type: FETCH_STATUS_SUCCESS,
    payload: { id, name, status },
  });
};

export const setStatusServerFailure = error => ({
  type: FETCH_STATUS_FAILURE,
  payload: { error },
});

export function removeErrorAlert() {
  return { type: REMOVE_ERROR };
}
