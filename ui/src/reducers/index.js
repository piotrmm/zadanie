import {
  FETCH_DATA_BEGIN,
  FETCH_DATA_SUCCESS,
  FETCH_DATA_FAILURE,
  FETCH_STATUS_BEGIN,
  FETCH_STATUS_SUCCESS,
  FETCH_STATUS_FAILURE,
  REMOVE_ERROR,
} from '../constants/action-types';

const initialState = {
  servers: [],
  loading: false,
  error: null,
};

function rootReducer(state = initialState, action) {
  switch(action.type) {
    case FETCH_DATA_BEGIN:
      return {
        ...state,
        loading: false,
        error: null,
      };

    case FETCH_DATA_SUCCESS:
      return {
        ...state,
        loading: false,
        servers: action.payload,
      };

    case FETCH_DATA_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload.error,
        servers: [],
      };

    case FETCH_STATUS_BEGIN:
      return {
        ...state,
        loading: false,
        error: null,
      };

    case FETCH_STATUS_SUCCESS:
      return {
        ...state,
        loading: false,
        servers: state.servers.map(
          (server) => server.id === action.payload.id
            ? {...server, ...action.payload}
            : server
        ),
      };

    case FETCH_STATUS_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload.error,
        servers: [],
      };

    case REMOVE_ERROR:
      return {
        ...state,
        error: null,
      };

    default:
      return state;
  }
};

export default rootReducer;
